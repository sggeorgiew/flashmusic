﻿using System;

namespace FlashMusic.Models
{
    public class ProgressReport
    {
        public ProgressAction ProgressAction { get; set; }

        public double PercentageComplete { get; set; }
    }

    public enum ProgressAction
    {
        MoveOut,
        MoveIn
    }
}
