﻿using System;

namespace FlashMusic.Models
{
    public class TileViewModel
    {
        public string Title { get; set; }

        public string Count { get; set; }
    }
}
