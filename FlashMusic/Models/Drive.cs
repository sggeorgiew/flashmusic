﻿using System;

namespace FlashMusic.Models
{
    public class Drive
    {
        public string Name { get; set; }

        public string VolumeLabel { get; set; }

        public string DriveFormat { get; set; }

        public override string ToString()
        {
            return string.Format($"{VolumeLabel} ({Name})");
        }
    }
}
