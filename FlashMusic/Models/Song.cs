﻿using System;

namespace FlashMusic.Models
{
    public class Song : IModel
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public long Length { get; set; }
    }
}
