﻿using FlashMusic.Windows;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlashMusic.Views
{
    /// <summary>
    /// Interaction logic for SelectDriveView.xaml
    /// </summary>
    public partial class AddIconView : UserControl
    {
        public AddIconView()
        {
            InitializeComponent();
        }

        private void SelectDrive()
        {
            new DriveSelectWindow().ShowDialog();
        }

        #region Events
        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SelectDrive();
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            SelectDrive();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.Close();
        }
        #endregion
    }
}
