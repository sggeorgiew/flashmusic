﻿using FlashMusic.Helpers;
using FlashMusic.Models;
using FlashMusic.Services;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Security;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlashMusic.Views
{
    /// <summary>
    /// Interaction logic for MusicGridView.xaml
    /// </summary>
    public partial class MusicGridView : UserControl
    {
        private string SelectedDrive;
        private string SelectedDirectory;

        private bool IsEditing { get; set; }
        private bool IsDragging { get; set; }

        private ObservableCollection<Song> Songs;
        private ProgressDialogController ProgressController;
        private SongService SongService;

        private Song DraggedSong
        {
            get { return (Song)GetValue(DraggedSongProperty); }
            set { SetValue(DraggedSongProperty, value); }
        }

        private static readonly DependencyProperty DraggedSongProperty =
            DependencyProperty.Register("DraggedSong", typeof(Song), typeof(MusicGridView));

        public MusicGridView(string selectedDrive, string selectedDirectory)
        {
            InitializeComponent();

            Loaded += WindowLoaded;

            this.SelectedDrive = selectedDrive;
            this.SelectedDirectory = selectedDirectory;

            SongService = new SongService(selectedDrive, selectedDirectory);
        }

        private async void WindowLoaded(object sender, RoutedEventArgs e)
        {
            await LoadFiles();
        }

        private void SelectFolder()
        {
            MainWindow.Instance.MainPanel.Children.RemoveAt(0);
            FolderTileView folderTileView = new FolderTileView(SelectedDrive);
            MainWindow.Instance.MainPanel.Children.Add(folderTileView);
        }

        private async Task LoadFiles()
        {
            string message = null;

            try
            {
                Songs = SongService.GetSongs();
                MusicGrid.ItemsSource = Songs;
            }
            catch (DirectoryNotFoundException)
            {
                message = "The selected directory was not found.";
            }
            catch (IOException)
            {
                message = "There is a problem with the paths.";
            }
            catch (ArgumentNullException)
            {
                message = "Problem with the extensions.";
            }
            catch (SecurityException)
            {
                message = "You dont have access to this folder.";
            }
            catch
            {
                message = "There is a problem with reading the files! Please try again.";
            }
            finally
            {
                if (message != null)
                {
                    await MainWindow.Instance.ShowMessageAsync($"An error occurred", $"{message}");
                }
            }
        }

        private async Task SortSongsAsync()
        {
            ProgressController = await MainWindow.Instance.ShowProgressAsync("Please wait...", "Sorting your songs.");

            Progress<ProgressReport> progress = new Progress<ProgressReport>();
            progress.ProgressChanged += ReportProgress;

            string message = null;

            try
            {
                await SongService.SaveSongsAsync(Songs, progress);
            }
            catch (PathTooLongException)
            {
                message = "The selected path is too long!";
            }
            catch (IOException)
            {
                message = "There is a problem with the paths.";
            }
            catch
            {
                message = "There is a problem with the saving! Please try again.";
            }
            finally
            {
                progress.ProgressChanged -= ReportProgress;

                if (message != null)
                {
                    await MainWindow.Instance.ShowMessageAsync($"An error occurred", $"{message}");
                }

                await ProgressController.CloseAsync();
            }
        }

        private async Task RemoveSongAsync()
        {
            if (!(MusicGrid.SelectedItem is Song selectedSong)) return;

            MessageDialogResult dialogResult;
            dialogResult = await MainWindow.Instance.ShowMessageAsync("Remove", $"Are you sure you want to remove \"{selectedSong.Title}\"?", MessageDialogStyle.AffirmativeAndNegative);

            if (dialogResult == MessageDialogResult.Negative) return;

            try
            {
                var songFile = Path.Combine(SelectedDrive, SelectedDirectory, selectedSong.Title);
                if (File.Exists(songFile))
                {
                    File.Delete(songFile);
                    Songs.Remove(selectedSong);
                }
            }
            catch (IOException)
            {
                await MainWindow.Instance.ShowMessageAsync($"An error occurred", "There is a problem with the file!");
            }
            catch (UnauthorizedAccessException)
            {
                await MainWindow.Instance.ShowMessageAsync($"An error occurred", "You don't have access to this file!");
            }
            catch
            {
                await MainWindow.Instance.ShowMessageAsync($"An error occurred", "Unexpected error occurred!");
            }
        }

        private void ReportProgress(object sender, ProgressReport e)
        {
            ProgressController.SetProgress(e.PercentageComplete);
            string action = e.ProgressAction == ProgressAction.MoveIn ? "Sorting" : "Moving";
            ProgressController.SetMessage($"{action} song {e.PercentageComplete * Songs.Count}/{Songs.Count}...\nThis can take a while.. Please be patient! :)");
        }

        private void ResetDragDrop()
        {
            IsDragging = false;
            popup.IsOpen = false;
            MusicGrid.IsReadOnly = false;
        }

        #region Events

        #region Grid
        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (!IsDragging || e.LeftButton != MouseButtonState.Pressed) return;

            if (!popup.IsOpen)
            {
                // Switch to Read-only mode
                MusicGrid.IsReadOnly = true;
                popup.IsOpen = true;
            }

            Size popupSize = new Size(popup.ActualWidth, popup.ActualHeight);
            popup.PlacementRectangle = new Rect(e.GetPosition(this), popupSize);

            // Make sure the row under the grid is being selected
            Point position = e.GetPosition(MusicGrid);
            var row = UIHelpers.TryFindFromPoint<DataGridRow>(MusicGrid, position);
            if (row != null) MusicGrid.SelectedItem = row.Item;
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!IsDragging || IsEditing) return;

            Song targetItem = MusicGrid.SelectedItem as Song;

            if (targetItem == null || !ReferenceEquals(DraggedSong, targetItem))
            {
                var targetIndex = Songs.IndexOf(targetItem);

                if (targetIndex != -1)
                {
                    Songs.Remove(DraggedSong);

                    Songs.Insert(targetIndex, DraggedSong);

                    MusicGrid.SelectedItem = DraggedSong;
                }                
            }

            ResetDragDrop();
        }
        #endregion

        #region DataGrid (Music)
        private void MusicGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsEditing) return;

            // Find the Selected Row
            var row = UIHelpers.TryFindFromPoint<DataGridRow>((UIElement)sender, e.GetPosition(MusicGrid));
            if (row == null) return;

            // Set flag that indicates we're capturing Mouse Movements
            IsDragging = true;
            DraggedSong = (Song)row.Item;
        }

        private void OnBeginEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            IsEditing = true;

            // In case we are in the middle of a drag/drop operation
            if (IsDragging) ResetDragDrop();
        }

        private async void OnEndEdit(object sender, DataGridCellEditEndingEventArgs e)
        {
            IsEditing = false;

            // On updating Song Name
            string oldSongName = (e.EditingElement.DataContext as Song).Title;
            string newSongName = (e.EditingElement as TextBox).Text;

            if (oldSongName != newSongName)
            {
                string errorMessage = null;

                try
                {
                    SongService.RenameSong(oldSongName, newSongName);
                }
                catch (IOException)
                {
                    errorMessage = "A file with the same name already exists!";
                }
                catch (UnauthorizedAccessException)
                {
                    errorMessage = "You don't have access to this file!";
                }
                finally
                {
                    if (errorMessage != null)
                    {
                        // Return the previous song name
                        (e.EditingElement as TextBox).Text = oldSongName;
                        ResetDragDrop();
                        await MainWindow.Instance.ShowMessageAsync($"An error occurred", $"{errorMessage}");
                    }
                }
            }
        }
        #endregion

        #region Buttons
        private void ChangeFolderButton_Click(object sender, RoutedEventArgs e)
        {
            SelectFolder();
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            await LoadFiles();
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            await SortSongsAsync();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Instance.Close();
        }
        #endregion

        #region Context Menu
        private async void RemoveSongContextMenu_Click(object sender, RoutedEventArgs e)
        {
            await RemoveSongAsync();
        }

        #endregion

        #endregion
    }
}
