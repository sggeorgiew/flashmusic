﻿using FlashMusic.Windows;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Windows;
using System.Windows.Controls;

namespace FlashMusic.Views
{
    /// <summary>
    /// Interaction logic for FolderTileView.xaml
    /// </summary>
    public partial class FolderTileView : UserControl
    {
        private string SelectedDrive;

        public FolderTileView(string selectedDrive)
        {
            InitializeComponent();

            this.SelectedDrive = selectedDrive;
            LoadFolders();
        }

        private async void LoadFolders()
        {
            FolderPanel.Children.Clear();

            string message = null;

            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(SelectedDrive);
                var directories = directoryInfo.GetDirectories();

                if (directories.Any())
                {
                    foreach (var directory in directories)
                    {
                        // Skip the hidden and system folders
                        if ((directory.Attributes & FileAttributes.System) != 0) continue;

                        TileView tile = new TileView(directory.Name, SelectedDrive);
                        FolderPanel.Children.Add(tile);
                    }
                }
            }
            catch (IOException)
            {
                message = "There is a problem with the paths.";
            }
            catch (UnauthorizedAccessException)
            {
                message = "You don't have access to this folder.";
            }
            catch (SecurityException)
            {
                message = "There is a problem with the folder security.";
            }
            catch
            {
                message = "Unexpected error occurred!";
            }
            finally
            {
                if (message != null)
                {
                    await MainWindow.Instance.ShowMessageAsync($"An error occurred", $"{message}");
                }
            }
        }

        private void ChangeDriveButton_Click(object sender, RoutedEventArgs e)
        {
            DriveSelectWindow driveSelectWindow = new DriveSelectWindow();
            var result = driveSelectWindow.ShowDialog();

            if (result.HasValue && result.Value)
            {
                SelectedDrive = MainWindow.Instance.SelectedDrive;
                LoadFolders();
            }
        }
    }
}
