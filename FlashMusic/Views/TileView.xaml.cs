﻿using FlashMusic.Models;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Controls;

namespace FlashMusic.Views
{
    /// <summary>
    /// Interaction logic for TileView.xaml
    /// </summary>
    public partial class TileView : UserControl
    {
        TileViewModel tileViewModel;

        public TileView(string folder, string drive)
        {
            InitializeComponent();

            tileViewModel = new TileViewModel
            {
                Title = drive,
                Count = folder
            };

            FolderTile.DataContext = tileViewModel;
        }

        #region Events
        private void FolderTile_Click(object sender, RoutedEventArgs e)
        {
            Tile tile = (Tile)sender;
            MainWindow.Instance.SelectedDirectory = tile.Count;
        }
        #endregion
    }
}
