﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashMusic.Helpers
{
    public static class Constants
    {
        public const string TempFolder = "D:\\FlashMusicTemp\\";

        public static readonly string[] SongExtensions = { ".wav", ".mp3", ".aac", ".wma", ".mp4" };
    }
}
