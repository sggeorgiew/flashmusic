﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashMusic.Extensions
{
    public static class DirectoryInfoExtension
    {
        public static IEnumerable<FileInfo> GetFilesByExtensions(this DirectoryInfo directoryInfo, params string[] extensions)
        {
            if (extensions == null)
                throw new ArgumentNullException("No extensions found");

            IEnumerable<FileInfo> files = directoryInfo.EnumerateFiles();
            return files.Where(f => extensions.Contains(f.Extension, StringComparer.OrdinalIgnoreCase));
        }
    }
}
