﻿using FlashMusic.Models;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;

namespace FlashMusic.Windows
{
    /// <summary>
    /// Interaction logic for DriveSelectWindow.xaml
    /// </summary>
    public partial class DriveSelectWindow
    {
        private ObservableCollection<Drive> RemovableDrives;

        public DriveSelectWindow()
        {
            InitializeComponent();

            RemovableDrives = new ObservableCollection<Drive>();
            RemovableDrivesComboBox.ItemsSource = RemovableDrives;

            Loaded += WindowLoaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            ScanRemovableDrives();
        }

        private void ScanRemovableDrives()
        {
            var drivesInfo = DriveInfo.GetDrives()
                .Where(drive => drive.IsReady && drive.DriveType == DriveType.Removable);

            foreach (var driveInfo in drivesInfo)
            {
                if (!RemovableDrives.Any(x => x.Name == driveInfo.Name))
                {
                    Drive drive = new Drive()
                    {
                        Name = driveInfo.Name,
                        VolumeLabel = driveInfo.VolumeLabel,
                        DriveFormat = driveInfo.DriveFormat
                    };

                    RemovableDrives.Add(drive);
                }
            }

            if (RemovableDrives.Any())
            {
                RemovableDrivesComboBox.SelectedIndex = 0;
                SelectDriveButton.IsEnabled = true;
            }
            else
            {
                // TODO: We couldn't found any removable drive. Please plug in it and scan again!
                SelectDriveButton.IsEnabled = false;
            }
        }

        #region Events
        private void ScanDriveButton_Click(object sender, RoutedEventArgs e)
        {
            ScanRemovableDrives();
        }

        private void SelectDriveButton_Click(object sender, RoutedEventArgs e)
        {
            Drive selectedItem = (RemovableDrivesComboBox.SelectedItem as Drive);

            if (!string.IsNullOrEmpty(selectedItem.Name))
            {
                MainWindow.Instance.SelectedDrive = selectedItem.Name;
                DialogResult = true;
            }
        }

        private void RemovableDrivesComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DriveFormatLabel.Content = "Drive format: " + (e.AddedItems[0] as Drive).DriveFormat;
        }
        #endregion
    }
}
