﻿using FlashMusic.Views;
using System.Diagnostics;
using System.Windows;

namespace FlashMusic
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private string selectedDrive;
        private string selectedDirectory;

        public string SelectedDirectory
        {
            get { return selectedDirectory; }
            set
            {
                selectedDirectory = value;
                OnDirectoryChanged();
            }
        }

        public string SelectedDrive
        {
            get { return selectedDrive; }
            set
            {
                selectedDrive = value;
                OnDriveChanged();
            }
        }
        
        public static MainWindow Instance;

        public MainWindow()
        {
            InitializeComponent();

            Instance = this;

            Loaded += WindowLoaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            AddIconView iconView = new AddIconView();
            MainPanel.Children.Add(iconView);
        }
        
        private void OnDirectoryChanged()
        {
            MainPanel.Children.RemoveAt(0);

            MusicGridView musicGridView = new MusicGridView(selectedDrive, selectedDirectory);
            MainPanel.Children.Add(musicGridView);
        }

        private void OnDriveChanged()
        {
            MainPanel.Children.RemoveAt(0);
            FolderTileView folderTileView = new FolderTileView(selectedDrive);
            MainPanel.Children.Add(folderTileView);
        }
        
        #region Right Window Buttons
        private void DonateWindowButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://www.paypal.me/SGeorgiew");
        }

        private void GitWindowButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://gitlab.com/sgeorgiew");
        }
        #endregion
    }
}
