﻿using FlashMusic.Extensions;
using FlashMusic.Helpers;
using FlashMusic.Models;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;

namespace FlashMusic.Services
{
    public class SongService
    {
        private readonly string SelectedDrive;
        private readonly string SelectedDirectory;

        public SongService(string selectedDrive, string selectedDirectory)
        {
            SelectedDrive = selectedDrive;
            SelectedDirectory = selectedDirectory;
        }

        public ObservableCollection<Song> GetSongs()
        {
            ObservableCollection<Song> songs = new ObservableCollection<Song>();
            string sourcePath = Path.Combine(SelectedDrive, SelectedDirectory);
            
            DirectoryInfo directoryInfo = new DirectoryInfo(sourcePath);

            var songFiles = directoryInfo.GetFilesByExtensions(Constants.SongExtensions);

            int id = 1;
            foreach (var file in songFiles)
            {
                Song song = new Song
                {
                    Id = id++,
                    Title = file.Name,
                    Length = file.Length
                };

                songs.Add(song);
            }

            return songs;
        }        

        public async Task SaveSongsAsync(ObservableCollection<Song> songs, IProgress<ProgressReport> progress)
        {
            await MoveSongsAsync(songs, progress);

            ProgressReport report = new ProgressReport();

            int counter = 0;

            foreach (var song in songs)
            {
                counter++;
                double value = (double)counter / songs.Count;

                report.ProgressAction = ProgressAction.MoveIn;
                report.PercentageComplete = value;
                progress.Report(report);

                string currentLocation = Path.Combine(Constants.TempFolder, song.Title);
                string newLocation = Path.Combine(SelectedDrive, SelectedDirectory, song.Title);

                File.Move(currentLocation, newLocation); // Try to move
                await Task.Delay(10);
            }

            if (Directory.Exists(Constants.TempFolder))
            {
                Directory.Delete(Constants.TempFolder);
            }
        }

        public void RenameSong(string oldName, string newSong)
        {
            string directoryPath = Path.Combine(SelectedDrive, SelectedDirectory);
            string oldPath = Path.Combine(directoryPath, oldName);
            string newPath = Path.Combine(directoryPath, newSong);
            
            if (File.Exists(newPath)) throw new IOException("File already exists");
            
            File.Move(oldPath, newPath);
        }

        private async Task MoveSongsAsync(ObservableCollection<Song> songs, IProgress<ProgressReport> progress)
        {
            ProgressReport report = new ProgressReport();

            if (!Directory.Exists(Constants.TempFolder))
            {
                Directory.CreateDirectory(Constants.TempFolder);
            }

            int counter = 0;

            foreach (var song in songs)
            {
                counter++;
                double value = (double)counter / songs.Count;

                report.ProgressAction = ProgressAction.MoveOut;
                report.PercentageComplete = value;
                progress.Report(report);

                string currentLocation = Path.Combine(SelectedDrive, SelectedDirectory, song.Title);
                string newLocation = Path.Combine(Constants.TempFolder, song.Title);

                File.Move(currentLocation, newLocation); // Try to move
                await Task.Delay(10);
            }
        }
    }
}
